json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :date, :timeslot_id, :physician_id, :note, :diagnostic_code_id, :patient_id, :reason
  json.url appointment_url(appointment, format: :json)
end
