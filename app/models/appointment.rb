class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :physician
  belongs_to :timeslot
  belongs_to :invoice
  has_and_belongs_to_many :diagnostic_codes
  belongs_to :diagnostic_code

  def appointment_avail
  end
end