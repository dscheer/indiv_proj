class Invoice < ActiveRecord::Base
  belongs_to :patient
  belongs_to :appointment
  belongs_to :physician

  def generate
    @invoices = Invoice.all
  end

end

