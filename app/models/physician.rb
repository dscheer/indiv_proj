class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :invoices, through: :appointments
end
