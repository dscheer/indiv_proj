class DiagnosticCode < ActiveRecord::Base
  has_many :patients, through: :appointments
end
