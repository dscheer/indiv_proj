class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :date
      t.integer :timeslot_id
      t.text :note
      t.integer :diagnostic_code_id
      t.text :reason

      t.timestamps
    end
  end
end
