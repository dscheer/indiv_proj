class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.string :number
      t.float :cost

      t.timestamps
    end
  end
end
