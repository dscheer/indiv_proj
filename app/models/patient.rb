class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :invoices, through: :appointments
  has_many :diagnostic_codes, through: :appointments
end
