class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.float :amount
      t.integer :patient_id
      t.integer :appointment_id

      t.timestamps
    end
  end
end
