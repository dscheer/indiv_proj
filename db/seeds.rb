#patient data

Patient.create(name: 'James Robinson')
Patient.create(name: 'Bob Sanchez')
Patient.create(name: 'Ann Justus')
Patient.create(name: 'Joe Granger')
Patient.create(name: 'Helen Gray')
Patient.create(name: 'Myra Phelps')
Patient.create(name: 'Paula Reynolds')
Patient.create(name: 'Halee Rodriguez')
Patient.create(name: 'Candace Wall')
Patient.create(name: 'Wyatt Cabrera')
Patient.create(name: 'Louis Higgins')
Patient.create(name: 'Destiny Washington')
Patient.create(name: 'Tara Simon')
Patient.create(name: 'Ainsley May')
Patient.create(name: 'Meredith Hurst')
Patient.create(name: 'Noah Tillman')
Patient.create(name: 'Igor Delgado')
Patient.create(name: 'Price Walton')
Patient.create(name: 'Yetta Sellers')

#physician data

Physician.create(name: 'James Tuttle')
Physician.create(name: 'Inez Williams')
Physician.create(name: 'Gina  Smith')
Physician.create(name: 'Annie Rodriguez')
Physician.create(name: 'Fred Clark')
Physician.create(name: 'Chloe Gonzales')

#timeslot data

Timeslot.create(time: '8:00 AM')
Timeslot.create(time: '8:30 AM')
Timeslot.create(time: '9:00 AM')
Timeslot.create(time: '9:30 AM')
Timeslot.create(time: '10:00 AM')
Timeslot.create(time: '10:30 AM')
Timeslot.create(time: '11:00 AM')
Timeslot.create(time: '11:30 AM')
Timeslot.create(time: '12:00 PM')
Timeslot.create(time: '12:30 PM')
Timeslot.create(time: '1:00 PM')
Timeslot.create(time: '1:30 PM')
Timeslot.create(time: '2:00 PM')
Timeslot.create(time: '2:30 PM')
Timeslot.create(time: '3:00 PM')
Timeslot.create(time: '3:30 PM')
Timeslot.create(time: '4:00 PM')
Timeslot.create(time: '4:30 PM')

#load diagnostic codes from pipe-delimited file
directory = "db/codes/"
path = File.join(directory, "codes.txt")
open(path) do |codes|
  codes.read.each_line do |code|
    number, description, cost = code.chomp.split("|")
    DiagnosticCode.create(number: number, description: description, cost: cost)
  end
end

Appointment.create(date: '2014-11-27', timeslot_id: 1, physician_id: 1, note: 'Prescribed antibiotics and recommended showering.', diagnostic_code_id: 36, patient_id: 1, reason: 'itchy feeling in my lip')
Appointment.create(date: '2014-11-27', timeslot_id: 1, physician_id: 2, note: 'Prescribed antibiotics and recommended jogging.', diagnostic_code_id: 37, patient_id: 1, reason: 'itchy feeling in my eyelid')
Appointment.create(date: '2014-11-29', timeslot_id: 1, physician_id: 3, note: 'Prescribed antibiotics and recommended dancing.', diagnostic_code_id: 38, patient_id: 1, reason: 'itchy feeling in my ear')
Appointment.create(date: '2014-11-29', timeslot_id: 2, physician_id: 4, note: 'Prescribed antibiotics and recommended sleeping.', diagnostic_code_id: 39, patient_id: 1, reason: 'itchy feeling in my face')
Appointment.create(date: '2014-11-30', timeslot_id: 4, physician_id: 1, note: 'Prescribed antibiotics and recommended coding.', diagnostic_code_id: 40, patient_id: 1, reason: 'itchy feeling in my scalp')
Appointment.create(date: '2014-11-30', timeslot_id: 4, physician_id: 2, note: 'Prescribed antibiotics and recommended learning.', diagnostic_code_id: 41, patient_id: 1, reason: 'itchy feeling in my trunk')
Appointment.create(date: '2014-11-30', timeslot_id: 5, physician_id: 3, note: 'Prescribed antibiotics and recommended yoga.', diagnostic_code_id: 42, patient_id: 1, reason: 'itchy feeling in my arm')
Appointment.create(date: '2014-11-30', timeslot_id: 7, physician_id: 5, note: ' Prescribed antibiotics and recommended tennis.', diagnostic_code_id: 43, patient_id: 1, reason: 'itchy feeling in my leg')
Appointment.create(date: '2014-11-30', timeslot_id: 13, physician_id: 1, note: 'Prescribed antibiotics and recommended hiking.', diagnostic_code_id: 44, patient_id: 1, reason: 'itchy feeling')
Appointment.create(date: '2014-11-30', timeslot_id: 15, physician_id: 3, note: 'Prescribed antibiotics and recommended music.', diagnostic_code_id: 45, patient_id: 1, reason: 'rash')

Appointment.create(date: '2014-12-03', timeslot_id: 1, physician_id: 1, note: 'Prescribed antibiotics and recommended music.', diagnostic_code_id: 48, patient_id: 1, reason: 'acne')
Appointment.create(date: '2014-12-03', timeslot_id: 2, physician_id: 2, note: 'Prescribed antibiotics and recommended music.', diagnostic_code_id: 20, patient_id: 2, reason: 'really bad sunburn')
Appointment.create(date: '2014-12-03', timeslot_id: 3, physician_id: 3, note: 'Prescribed antibiotics and recommended music.', diagnostic_code_id: 7, patient_id: 3, reason: 'bruise wont heal')







